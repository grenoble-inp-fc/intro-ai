---
customTheme: "mytheme"
highlightTheme: "vs"
parallaxBackgroundImage: null
transition: concave
---

# AI
## an introduction

#### [Dominique Houzet](mailto:dominique.houzet@grenoble-inp.fr), [Thomas Hueber](mailto:thomas.hueber@grenoble-inp.fr), [Ronald Phlypo](mailto:ronald.phlypo@grenoble-inp.fr)

---

## What's AI ?
### a brief history

see also: [a history of AI](https://ahistoryofai.com)

--

### Aristotle (384 BC &ndash; 322 BC)
#### syllogistic logic

<div class="grid-container-right">
<div class="item1">
<ul>
<li> All men are mortal</li>
<li> Aristoteles is a man</li>
</ul><br />
&Rightarrow; Aristoteles is mortal
</div>
<div class="item2">
<img class="plain" src="./images/Intro/Aristotle.jpg" width="100%">
</div>
</div>

<cite>E.T. Jaynes &laquo;<a href="http://www-biba.inrialpes.fr/Jaynes/prob.html">Probability Theory &ndash; The Logic of Science,</a>&raquo; Cambridge University Press, 2003.</cite>

--

### Ismail Al-Jazari (1136 &ndash; 1206)

first (humanoïd) programmable automata

<cite>Ismaïl Al-Jazari: &laquo;<a href="https://muslimheritage.com/uploads/Automation_Robotics_in_Muslim%20Heritage.pdf">The Book of Knowledge of Ingenious Mechanical Devices</a>&raquo;</cite>

--

### Thomas Aquinas (1225 &ndash; 1274)
#### there is a reason for everything

<div class="grid-container-left">
<div class="item1">
<img src="./images/Intro/Acquino.jpg" width="100%">
</div>
<div class="item2"> 
<blockquote>the existence of God can be demonstrated by simple logic</blockquote>
</div>
</div>

--

### Thomas Hobbes (1588 &ndash; 1679)
#### no free will

<div class="grid-container-right">
<div class="item1">
<blockquote>By reasoning, I understand computation</blockquote>
</div>
<div class="item2">
<img src="./images/Intro/Hobbes.jpg" width="100%">
</div>
</div>

<cite>Thomas Hobbes: &laquo;<a href="https://archive.org/details/englishworkstho21hobbgoog/page/n9">De corpore</a>&raquo; (1655)</cite>

--

### René Descartes (1596 &ndash; 1650)
#### &ldquo;*Ego cogito, ergo sum*&rdquo;

<div class="grid-container-left">
<div class="item1">
<img src="./images/Intro/Descartes.jpg" width="100%">
</div>
<div class="item2">
<ul>
<li>evidence</li>
<li>enumeration</li>
<li>analysis</li>
<li>synthesis</li>
</ul>
</div>
</div>

<cite>René Descartes: &laquo;<a href="https://gallica.bnf.fr/ark:/12148/btv1b86069594.r=descartes%20discours%20de%20la%20m%C3%A9thode?rk=214593;2">Discours de la méthode</a>&raquo; (1637)</cite>

--

### Blaise Pascal (1623 &ndash; 1662)
#### axiomatic computing

<div class="grid-container-left">
<div class="item1">
<img src="./images/Intro/BlaisePascal.jpg" width="100%">
</div>
<div class="item2">
<blockquote>Il n'est pas certain que tout soit certain.</blockquote>
</div>
</div>

instinct, heart, feelings &leftrightarrow; reason<br />
* <i>axioma</i> &Rightarrow; truth values 
* reasoning = mechanistic

--

#### mechanic process of addition
##### binary addition = XOR + carry-over bit

|B1 |B2 |XOR | carry-over |
|-- |-- |-- |-- |
|0 |0 |0 |0 |
|0 |1 |1 |0 |
|1 |0 |1 |0 |
|1 |1 |0 |**1** |


--

#### mechanic process of addition
##### a three register operation

```
R1 : 001
R2 : 011
R3 : 000  # carry-over register
```

--

#### mechanic process of addition
##### pseudo-code

```
while R2 <> 0 
    R3 <- R1 and R2
    R3 << 
    R1 <- R1 xor R2
    R2 <- R3
    R3 <- 0
```

--

#### mechanic process of addition
##### an example

|iterate   | R1  | R2  | R3  |
|-- |--   |--   |--   |
|0 | 001 | 011 | 000 |
|1 | 010 | 010 | 000 |
|2 | 000 | 100 | 000 |
|3 | 100 | 000 | 000 | 

&check; artificial &cross; intelligence

<aside class="notes">
The 18th century saw a profusion of mechanical toys, including the celebrated mechanical duck of Vaucanson and von Kempelen's phony mechanical chess player, The Turk (1769). Edgar Allen Poe wrote (in the Southern Literary Messenger, April 1836) that the Turk could not be a machine because, if it were, it would not lose.
</aside>

--

### Gottfried Wilhelm Leibniz (1646 &ndash; 1716)

<div class="grid-container">
<div class="item1">
<img src="./images/Intro/Leibniz.jpg" width="100%">
</div>
<div class="item2">
<img src="./images/Intro/LeibnizMachine.jpg" width="100%">
Calculus ratiocinator
</div>
</div>

<aside class="notes">
algorithme ou une machine calculatoire théorique inventé par Gottfried Wilhelm Leibniz et décrit dans son ouvrage De arte combinatoria en 1666. On peut le voir comme une méthode, un algorithme, ou une machine, qui permettrait de démêler le vrai du faux dans toute discussion dont les termes seraient exprimés dans la langue philosophique universelle, la caractéristique universelle. Cette dernière, imaginée aussi par Leibniz sans qu'il l'ait complètement formalisée, était censée pouvoir exprimer n'importe quel énoncé philosophique. Leibniz imaginait donc un procédé automatique couplant la langue formalisée et l'algorithme, qui puisse décider de la vérité de toute assertion quelle qu'elle soit. 
</aside>

--

### Gottfried Wilhelm Leibniz (1646 &ndash; 1716)
#### his philosophy

* identity &leftrightarrow; contradiction
* identity of indescernibles
* sufficient reason: <i>There must be a sufficient reason for anything to exist, for any event to occur, for any truth to obtain</i>
* pre-established harmony: <i>intrinsic knowledge</i>
* law of continuity
* optimism
* plenitude

--

### Wolfgang von Kempelen (1734 &ndash; 1804)
#### Chess player

<section>
<img src="./images/Intro/mechTurk.jpg" width="50%"><br />
1770: the mechanical turk
</section>

--

### 19th century
#### an accelaration

<div class="grid-container-right">
<div class="item1">
Joseph-Marie Jacquard<br />(1752 &ndash; 1834)<br />
1804: the Jacquard loom
</div>
<div class="item2">
<img src="./images/Intro/Jacquardloom.jpg" width="100%">
</div>
</div>

<div class="grid-container-left">
<div class="item1">
<img src="./images/Intro/diffEngine.jpg" width="100%">
</div>
<div class="item2">
Charles Babbage (1791 &ndash; 1871)<br />
<ul>
<li>1822: difference engine</li>
<li>1837: analytic engine</li>
</ul>
</div>
</div>

--

### Bertrand Arthur William Russell
#### (1872 &ndash; 1970)
### Alfred North Whitehead
#### (1861 &ndash; 1947)

* Modern mathematical, symbolic logic &amp; its notation
* Russell's paradox (set logic) $$R=\\{x|x\notin x\\}\implies (R\in R\iff R\notin R)$$
  
<cite>B.A.W. Russell and A.N. whitehead &laquo;<a href="https://archive.org/details/PrincipiaMathematicaVolumeI">Principia Mathematica</a>&raquo; (3 volumes) 1910 &ndash; 1913</cite>

--

### Alan Turing (1912 &ndash; 1954)
#### birth of AI

<div class="grid-container-left">
<div class="item1">
<img src="./images/Intro/AlanTuring.jpg" width="100%">
</div>
<div class="item2">
<ul>
<li>Turing test</li>
<li>Turing machine</li>
<li>Bomber machine (cracks Enigma)</li>
</ul>
</div>
</div>

<cite>Alan Turing: &laquo;<a href="https://en.wikipedia.org/wiki/Computing_Machinery_and_Intelligence">Computing machinery and intelligence</a>&raquo; Mind, 1950.</cite>

--

### Some game-changing publications
#### The birth of AI

the birth of cybernetics

<cite>Arturo Rosenblueth, Norbert Wiener, Julian Bigelow: &laquo;<a href="./refs/1943_Rosenblueth_Wiener_Bigelow.pdf" target="_blank" rel="noopener noreferrer">Behavior, Purpose and Teleology</a>&raquo; Philosophy of science, Vol 10, n°1, pp18&ndash;24, 1943.</cite>
<cite>Norbert Wiener: &laquo;<a href="./refs/1948_Wiener.pdf">Cybernetics, or control and communication in the animal and the machine</a>The MIT Press, 1948.</cite><br />

the birth of artificial neurons

<cite>Warren S. McCulloch, Walter Pitts: &laquo;<a href="./refs/1943_Rosenblueth_Wiener_Bigelow.pdf" target="_blank" rel="noopener noreferrer">A logical calculus of the ideas immanent in nervous activity</a>&raquo; Bulletin of Mathematical Biophysics, Vol 5, pp. 115&ndash;133, 1943.</cite><br />

*for a more recent survey*

<cite>Laurence F. Abbott, Peter Dayan: &laquo;<a href="./refs/2001_Abbott.pdf" target="_blank" rel="noopener noreferrer">Theoretical neuroscience</a>&raquo; MIT University Press, 2001.</cite>

--

### Some game-changing publications
#### The birth of AI

the birth of game theory

<cite>John von Neumann, Oskar Morgenstern: &laquo;<a href="1944_VonNeumann_gametheory.pdf" target="_blank" rel="noopener noreferrer">Theory of Games and Economic Behavior</a> Princeton University Press, 1944.</cite><br />

introducing heuristics in the exploration of solution space based on four principles:
* understand the problem
* devise a plan
* carry out the plan
* review/extend

<cite>George P&oacute;lya: &laquo;<a href="https://en.wikipedia.org/wiki/How_to_Solve_It">How to solve it?</a>&raquo; Princeton University Press, 1945.</cite>

--

#### the heuristics of P&oacute;lya

|heuristic/description|formal|
|--|--|
|easier, analoguous problem|mapping|
|find a more general problem|generalisation|
|general problem from examples?|induction|
|new set of problems&rightarrow;problem in hand|subgoal|
|related, solved problem|pattern recognition/matching &amp; reduction|
|more specialised problem|specialisation|
|decompose &amp; recombine|divide and conquer|
|working backwards goal&rightarrow;known problem|backward chaining|
|picture of the problem|diagrammatic reasoning|
|new elements&rightarrow;approach solution|extension|

--

### Some game-changing publications
#### The birth of AI

<cite>Claude Shannon: &laquo;<a href="./refs/1950_Shannon.pdf" target="_blank" rel="noopener noreferrer">Programming a computer to play chess</a>&raquo; Philosophical Magazine, Series 7, Vol.41, n°314, 1950.</cite><br />

First AI program: solving the Principia Mathematica by a computer program similar to human reasoning

<cite>Allen Newell, Herbert A. Simon: <a href="./refs/1956_Newell.pdf" target="_blank" rel="noopener noreferrer">The logic theory machine: a complex information processing system</a>, The Rand corporation, 1956.</cite>

<aside class="notes">
https://ahistoryofai.com/
Newell and Simon later developed GPS, a first AI lab, and a unified theory of cognition
1963 John McCarthy started Project MAC, which would later become the MIT Artificial Intelligence Lab. The research would contribute to cognition, computer vision, decision theory, distributed systems, machine learning, multi-agent systems, neural networks, probabilistic inference, and robotics. Later that year, McCarthy and Marvin Minskey launched SAIL: Stanford Artificial Intelligence Laboratory. The research institute would pave the way for operating systems, artificial intelligence, and the theory of computation.
</aside>

--

### Artificial Intelligence
#### a new field of science

<blockquote>one generation for the problem to be solved</blockquote>

* **artificial intelligence**
  * 1956
  * conference @ Dartmouth College, Hanover, New Hampshire
  * term used by by John Mc Carthy, computer scientist

* **challenge**
  * unconscious processing &rightarrow; machine algorithm

<aside class="notes">
conference organised by McCarthy, cognitive scientist Marvin Minsky, computer scientist Nathan Rochester, and mathematician Claude Shannon
</aside>

--

### Artificial Intelligence
#### AI in the labs

**1963**

* John McCarthy: Project MAC &rightarrow; MIT Artificial Intelligence Lab
* Mc Carthy and Minskey: SAIL (Stanford Artificial Intelligence Laboratory)

<cite>Edward A. Feigenbaum, R.W. Watson: &laquo;<a href="./refs/1965_Feigenbaum.pdf" target="_blank" rel="noopener noreferrer">An initial problem statement for a machine induction research project</a>&raquo; Stanford Artificial Intelligence Project, Memo 30, 1965</cite>

--

### Going deeper
#### Artificial Neural Networks

A deep convolutional neural network for shift-invariance

<cite>Kunihiko Fukushima: &laquo;<a href="./refs/1980_Fukushima.pdf" target="_blank" rel="noopener noreferrer">Neocognitron: A Self-organizing Neural Network for a Mechanism of Pattern Recognition Unaffected by Shift in Position</a>&raquo; Biological Cybernetics, Vol.36, pp.193--202, 1980.</cite><br />

Neural networks for learning and memory (recursive neural networks)<br />

<blockquote>Neurons that fire together, wire together</blockquote>

<cite>Donald Olding Hebb: &laquo;<a href="https://en.wikipedia.org/wiki/Organization_of_Behavior">The organization of behavior</a>, New York, Wiley and sons, 1949.</cite><br />

<cite>John J. Hopfield: &laquo;<a href="./refs/1982_Hopfield.pdf" target="_blank" rel="noopener noreferrer">Neural networks and physical systems with emergent collective computational abilities</a>&raquo; Proc. of the Natl. Acad. Sci. USA, Vol.79, pp.2554&ndash;2558, 1982.</cite>

--

### ... and then came backpropagation
#### the chain rule in an artificial neural network

evaluating the backpropagating error gradient is as expensive as evaluating the function approximation itself

<cite>Yann Le Cun: &laquo;<a href="./refs/1985_LeCun.pdf" target="_blank" rel="noopener noreferrer">A learning scheme for assymetric threshold network</a>&raquo; Cognitiva 1985</cite><br />

<cite>David E. Rumelhart, Geoffrey E. Hinton, Ronald J. Williams: &laquo;<a href="./refs/1986_Rumelhart" target="_blank" rel="noopener noreferrer">Learning representations by backpropagating errors</a>&raquo; Letter to Nature, Nature Vol.323, 1986</cite>

<aside class="notes">
Hopfield=physicist 
Geoffrey Hinton and David Rumelhart=psychologists
</aside>

--

### Technology meets philosophy
#### Today's neural network architectures

* *Thomas* will tell you more about the deep learning networks and how they arose

* *Dominique* will illustrate the advantages of adapting hardware to the models

* I will focus on a simpler problem first

<section><i>machine learning</i></section>

---

## The philosophy of machine learning
### The what and the how

--

### What is *learning* ?
#### Reinforcement learning

<section>
<img src="./images/ML/reinforcement_learning.png" width="60%">
</section>

see also: <a href="https://skymind.ai/wiki/deep-reinforcement-learning#neural"><i>skymind.ai</i>'s intro to deep reinforcement learning</a>

--

### What is *learning* ?
#### Reinforcement learning

1. **goal**
   * an accomplished goal gives ultimate satisfaction
2. **actions** &rightarrow; interact with environment
   * the agent disposes of a set of actions $\mathcal A=\\{a_i, i\in\mathbb N\\}$
3. action &rightarrow; changes **state** of agent
   * state transitions $S(t)=s_k \to{A(t)=a_i}\to S(t+1)=s_m$
4. **reward**
   * state transition under action &rightarrow; reward $R_{a_i}(s_k, s_m)$
5. **policy**
   * determines choice of actions/state transitions
   * maximising short-/long-term rewards
   * $P_{a_i}(s_k,s_m) = \Pr(S(t+1)=s_m | S(t)=s_k, A(t)=a_i)$
6. **observer**
   * a ruleset/interpreter that determines the reward of the agent
   * moral observer, omniscient observer, &hellip;

--

### machine learning
#### interaction between the experimentalist and the machine

1. **goal**
   * predict data class label, predict parameter value, &hellip;
2. **reward**
   * energy function / objective function over domain
3. **state**
   * current values of model parameters
4. **actions**
   * descending in the energy potential field
   * gradient descent, stochastic gradient descent, stochastic search
5. **policy** (experimentalist)
   * energy minimisation *vs.* explorative search
   * which algorithm to choose for what data ?
6. **observer** (experimentalist)
   * performance of model

--

### machine learning
#### physical principle: minimise energy

<section>
<img src="./images/ML/optimisation.png" width="80%" height="80%">
</section>

--

### Different goals
#### What is a ML model?

* supervised, unsupervised or semi-supervised
  * supervised learning needs labelled samples (labour intensive)
  * unsupervised learning looks for structure in the feature space

* learning algorithm
  * plain gradient descent: &check; clean data
  * btach, momentum, stochastic descent: &check; real data

* data dimensions ($n$: samples, $p$: features, $m$: model parameters)
  * $n\gg p$: feature-based learning
  * $n\ll p$: example-based learning
  * $m\gg p$: regularisation methods (sparsity, energy, &hellip;)


--

### machine learning
#### computer science &cap; statistics &cap; applied maths &cap; hardware design

* $p$: dimension of data (features)
* $n$: number of samples
* $m$: number of model parameters

|regime|difficulty|
|--|--|
| $p\gg n$ | asymptotic statistics break down |
| $n\to\infty$ | classical programming breaks down |
| $m\gg p$ | classical optimisation breaks down |
| $m+pn\gg$ resources | classical computer hardware insufficient |

<cite>Beijing innovation center for future chips: &laquo;<a href="./refs/2018_Beijing_chips.pdf" target="_blank" rel="noopener noreferrer">White paper on AI chip technologies</a>&raquo;, 2018</cite>
<cite>Bradley Efron, Trevor Hastie: &laquo;<a href="http://web.stanford.edu/~hastie/CASI/order.html">Computer-age statistical inference</a>&raquo; Cambridge University Press, 2016.</cite>
<cite>S&ouml;ren Boyn et al.: &laquo;<a href="./refs/2016_Boyn_Garcia.pdf" target="_blank" rel="noopener noreferrer">Learning through ferroelectric domain dynamics in solid-state synapses</a>&raquo; Nature Communications, 2017</cite>
<cite>Carlos Zamarre&ntilde;o-Ramos et al.: &laquo;<a href="./refs/2011_STDP_memristor.pdf" target="_blank" rel="noopener noreferrer">On spike-timing-dependent-plasticity, memristive devices, and building a self-learning visual cortex</a>&raquo; frontiers in Neuroscience, Vol.5, Article 26, 2011</cite>

--

### machine learning
#### hands-on experience with python

&rightarrow; scikit-learn, tensorflow, keras<br />
&rightarrow; jupyter notebooks

launch a terminal with
```bash
cd $HOME/intro-ai/notebooks/
jupyter notebook
```