{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 0. Getting the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd  # data manipulation library\n",
    "import sklearn as sk  # scikit-learn base library\n",
    "from sklearn.model_selection import train_test_split  # allows to split datasets for machine learning purposes\n",
    "from sklearn import preprocessing  # preprocessing pipeline from scikit-learn\n",
    "import matplotlib.pyplot as plt  # imports a plotting library\n",
    "from matplotlib import style, rcParams  # setting session parameters for matplotlib\n",
    "import numpy as np  # numerical computing library\n",
    "import seaborn as sns  # fancy scientific/statistical plotting library"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "style.use('seaborn-poster')\n",
    "%matplotlib inline\n",
    "rcParams['figure.figsize'] = (12, 10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Load wine dataset into memory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset_url = 'http://mlr.cs.umass.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv'\n",
    "data = pd.read_csv(dataset_url, sep=';')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Features:\n",
    "\n",
    "    Alcohol\n",
    "    Malic acid\n",
    "    Ash\n",
    "    Alcalinity of ash\n",
    "    Magnesium\n",
    "    Total phenols\n",
    "    Flavanoids\n",
    "    Nonflavanoid phenols\n",
    "    Proanthocyanins\n",
    "    Color intensity\n",
    "    Hue\n",
    "    OD280/OD315 of diluted wines\n",
    "    Proline\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.datasets import load_wine\n",
    "X, y = load_wine(return_X_y=True)\n",
    "d = pd.DataFrame(np.hstack([X, y[:, None]]),\n",
    "             columns=['Alcohol', 'Malic Acid', 'Ash', 'Alcalinity of ash', 'Magnesium', 'Total phenols',\n",
    "                     'Flavanoids', 'Nonflavanoid phenols', 'Proanthocyanins', 'Color intensity',\n",
    "                     'Hue', 'OD280/OD315 of diluted wines', 'Proline', 'Producer'])\n",
    "\n",
    "d.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1. Looking at the data\n",
    "## What is the size of the dataset ?\n",
    "\n",
    "$n$: number of samples\n",
    "\n",
    "$p$: number of features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n, p = d.shape\n",
    "print('(n = {shape[0]}, p = {shape[1]})'.format(shape=d.shape))  # nb of examples (n) x nb of features (p)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First wine in list\n",
    "\n",
    "this will also give us the available features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d.loc[0]  # freely change 0 to any other index in {0, 1, ..., n-1}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Look at a particular feature of the dataset\n",
    "\n",
    "* marginal distribution of features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d.plot.box(vert=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Looking at the feature distributions | label\n",
    "\n",
    "* what is the relationship between (normalized) features and labels ?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d['Alcohol'].hist(by=d['Producer'], bins=25, alpha=.5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "standardize = lambda x: (x-x.mean()) / x.std()\n",
    "zdata = d.pipe(standardize)\n",
    "zdata['Producer'] = d['Producer']\n",
    "zdata.boxplot(vert=False, by='Producer')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Look at feature interactions\n",
    "\n",
    "* some quasi independent features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.plotting.scatter_matrix(d[['Total phenols', 'Alcohol']], figsize=(12, 12), diagonal='kde')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* some (clearly) dependent features"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "pd.plotting.scatter_matrix(d[['Malic Acid', 'Alcalinity of ash', 'Hue']], figsize=(12, 12), diagonal='kde')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2. Set-up data for learning\n",
    "## Quality (label) is to be predicted from $p-1$ chemical measurements (features)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = d['Producer'].astype(int)\n",
    "X = d.drop('Producer', axis=1)\n",
    "X.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## split data into train and test set\n",
    "\n",
    "* we want equally distributed labels in all data &rightarrow; stratify the label $y$\n",
    "* 80% of the data to train, 20% to test"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test, y_train, y_test = train_test_split(X, y, \n",
    "                                                    test_size=0.2, \n",
    "                                                    random_state=123,  # to guarantee reproducibility ! \n",
    "                                                    stratify=y)\n",
    "X_train.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n0, q = np.histogram(y_train, bins=[0, 1, 2, 3], density=True)\n",
    "n1, q = np.histogram(y_test, bins=[0, 1, 2, 3], density=True)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.bar(q[:-1]-.20, n0, .40, label='train')\n",
    "ax.bar(q[:-1]+.20, n1, .40, label='test')\n",
    "ax.set_title('distribution of the quality over test/train set')\n",
    "ax.set_xlabel('quality')\n",
    "ax.set_ylabel('relative frequency')\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## standardize data\n",
    "\n",
    "* we have seen that different features have different extent &rightarrow; standardise data\n",
    "* only 'observed' data can be used &rightarrow; parameter learning limited to training data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scaler = preprocessing.StandardScaler().fit(X_train)\n",
    "\n",
    "Z_train = scaler.transform(X_train)\n",
    "Z_test = scaler.transform(X_test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3. A first attempt : unsupervised learning\n",
    "\n",
    "does not use labels, only features\n",
    "\n",
    "## 3.1 Clustering / dimension reduction\n",
    "### Goal\n",
    "\n",
    "Regroup samples based on their similarity &rightarrow; clustering, dimension reduction\n",
    "\n",
    "### Principle\n",
    "\n",
    "minimise inter-class similarity, maximise intra-class similarity\n",
    "\n",
    "### Methods\n",
    "\n",
    "K-means, (Gaussian) mixture modelling, classification trees, mean shift algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.2 Low-dimensional data embedding / relational learning\n",
    "### Goal\n",
    "\n",
    "find an underlying data structure &rightarrow; data visualisation, feature space exploration\n",
    "\n",
    "### Principle\n",
    "\n",
    "describe the structure based on low-dimensional embedding (data live on a low-dimensional structure, observations are noisy deviations from that structure)\n",
    "\n",
    "### Methods\n",
    "\n",
    "spectral embedding (Laplacian, isomap, Hessian), self-organising maps, manifold learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3.3 Focus on _self-organising maps_\n",
    "\n",
    "* features &rightarrow; topographic map\n",
    "* similar to topographic mapping in the brain\n",
    "* training by association\n",
    "* _goal_: find topographic map associated with feature space\n",
    "\n",
    "drawback: very sensitive to distribution of samples\n",
    "\n",
    "pseudo-code\n",
    "```\n",
    "input:\n",
    "    (m, n): map dimensions\n",
    "    p: dimension of feature space\n",
    "    eta: learning-rate\n",
    "    sigma: radius for determining neighbourhood\n",
    "    iter: number of iterations\n",
    "\n",
    "1. initialise weight tensor W randomly (p, m, n), i.e., m*n weight vectors wk\n",
    "   normalise weights\n",
    "   \n",
    "2. do while i < iter:\n",
    "    choose random input vector xi\n",
    "    get closest point on map k = argmin ||xi - wk||\n",
    "    determine the neighbourhood function hj(k)\n",
    "    update all weight vectors wj <- wj + eta * h_j(k) * xi\n",
    "    re-normalise wj <- wj / ||wj||\n",
    "    \n",
    "output:\n",
    "    learned topographic map as a network with weights W\n",
    "```\n",
    "\n",
    "&Rightarrow; seems this is tensor-like !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import som\n",
    "from importlib import reload\n",
    "reload(som)\n",
    "som_ = som.SOM(5, 5, X.shape[1], 0.1, 2, 100)\n",
    "som_.train(Z_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "yhat_test = som_.map_input(Z_test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "labels = [np.array(u) for u in zip(*yhat_test)]\n",
    "for h in np.unique(y_test):\n",
    "    ax.scatter(x=labels[0][y_test==h]+(h//2)/10, y=labels[1][y_test==h]+(h%2)/10, label='{}'.format(h))\n",
    "    # ax.set_xlim([0, 9])\n",
    "    # ax.set_ylim([0, 9])\n",
    "ax.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Looks like the problem will not be too hard to solve, since clustering gives natural clusters for producers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4. A second attempt : supervised learning\n",
    "\n",
    "Two major classes: regression and classification\n",
    "\n",
    "## 4.1 Regression\n",
    "\n",
    "### Goal\n",
    "\n",
    "* predicting a continuous variable from the features\n",
    "\n",
    "### Principle\n",
    "\n",
    "* minimise loss usually given by $f(\\hat y, y)$, where $y$ is the true value to predict and $\\hat y$ is the current predicted value (depends on the current values of the features)\n",
    "\n",
    "### Examples\n",
    "\n",
    "logistic regression, gaussian processes, generalised linear models (ridge regr., lasso, elastic net, &hellip;)\n",
    "\n",
    "## 4.2 Classification\n",
    "\n",
    "### Goal\n",
    "\n",
    "* predicting a class from the features\n",
    "\n",
    "### Principle\n",
    "\n",
    "* loss is usually an information-theoretic measure of cross-entropy of alike, penalising miss-classification when predicting the probability of the classes based on the current features values\n",
    "\n",
    "### Examples\n",
    "\n",
    "multi-layer perceptron, nearest-neighbours, naive Bayes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4.3 Focus on multi-layer perceptron\n",
    "\n",
    "* hidden/activation layers\n",
    "    * ReLU (rectifying linear unit)\n",
    "    * sigmoid, tanh\n",
    "    * &hellip;\n",
    "\n",
    "* output layer\n",
    "    * softmax\n",
    "\n",
    "$$\\hat p_i = \\frac{\\exp(z_i)}{\\sum_j \\exp(z_j)} \\in [0, 1]$$\n",
    "\n",
    "* objective function\n",
    "    * cross-entropy where $p_i = \\Pr(C=i | features)$, in training $p_j=1$ and $p_{i\\neq j}=0$\n",
    "\n",
    "$$H = -\\sum_i p_i \\log \\hat p_i$$\n",
    "\n",
    "* regularisation\n",
    "    * f = H + \\alpha \\|W\\|_2^2\n",
    "    * add $\\ell_2$ regularisation term on weights to objective function\n",
    "    * faster, more stable convergence\n",
    "\n",
    "* multiple algorithms for learning\n",
    "    * gradient descent L-BFGS (Broyden - Fletcher - Goldfarb - Shanno), L stands for limited-memory: if small dataset \n",
    "    * gradient descent with momentum (ADAM): if lots of data\n",
    "    * stochastic gradient descent: if well-tuned regularisation parameter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.neural_network import MLPClassifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "n_samples = 20\n",
    "labels = y_test.unique()\n",
    "l_ = np.random.randint(labels.size, size=(n_samples, ))\n",
    "ix = [np.where(y==l)[0] for l in labels]\n",
    "print([np.random.choice(ix[l]) for l in l_])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clf = MLPClassifier(hidden_layer_sizes=(50, 10), max_iter=10000, activation='relu', solver='adam', \n",
    "                    alpha=1e-2, momentum=.3)  # expansion, bottleneck, expansion\n",
    "\n",
    "clf.fit(Z_train, y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "P = clf.predict(Z_test)\n",
    "sns.jointplot(P, y_test, kind='kde')\n",
    "print('Training score: {}\\nTesting score  :{}'.format(clf.score(Z_train, y_train), clf.score(Z_test, y_test)))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
